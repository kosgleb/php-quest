<?php

namespace App\Providers;

use App\Support\ComponentsManager;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\ServiceProvider;
use Phalcon\Paginator\Adapter\Model;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     *
     * @throws BindingResolutionException
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app['config']->set('app.aliases', [
                'Eloquent' => Model::class,
            ]);
            $this->app->register(IdeHelperServiceProvider::class);
        }
        $this->app->singleton(ComponentsManager::class);
        /** @var ComponentsManager $manager */
        $manager = $this->app->make(ComponentsManager::class);
        $manager->registerComponents();
    }

    public function boot()
    {
        Resource::withoutWrapping();
    }
}
