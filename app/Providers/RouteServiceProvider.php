<?php

namespace App\Providers;

use App\Support\ComponentsManager;
use Illuminate\Support\ServiceProvider;
use Laravel\Lumen\Application;

class RouteServiceProvider extends ServiceProvider
{

    /** @var ComponentsManager $componentsManager */
    protected $componentsManager;

    /** @var Application */
    protected $app;

    /**
     * RouteServiceProvider constructor.
     *
     * @param $app
     */
    public function __construct($app)
    {
        parent::__construct($app);
        $this->componentsManager = $this->app->make(ComponentsManager::class);
    }

    public function register()
    {
        foreach ($this->componentsManager->getRoutes() as $routePath) {
            $this->app->router->group([], function ($router) use ($routePath) {
                if (file_exists($routePath)) {
                    return require $routePath;
                }
            });
        }
    }

}