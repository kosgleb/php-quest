<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * Class Controller
 * @package App\Http\Controllers
 *
 * @OA\Info(
 *     title="Lamoda PHP Quest",
 *     version="0.1"
 * )
 * @OA\Server(url="http://localhost:8080")
 */
class Controller extends BaseController
{
    //
}
