<?php

namespace App\Console\Commands;

use App\Components\Containers\Models\Container;
use App\Components\Products\Models\Product;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class SeedDataBaseCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'containers:seed {--container_size=10} {--containers_count=1000} {--products_count=100} {--no-refresh}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Команда сидит базу товарами, контейнерами и связывает их';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!$this->option('no-refresh')) {
            $this->line('Refreshing database');
            Artisan::call('migrate:fresh');
        }
        $productsCount = (int) $this->option('products_count');
        $containersCount = (int) $this->option('containers_count');
        $containerSize = (int) $this->option('container_size');
        $this->line("Seeding database with $productsCount products, $containersCount containers. Container size: $containerSize");
        $this->line('Seeding products table');
        factory(Product::class, $productsCount)->create();
        $this->line('Seeding products table: done');
        $this->line('Seeding containers table');
        factory(Container::class, $containersCount)->create()->each(function (Container $container) use ($productsCount, &$page, $containerSize) {
            $offset = $this->calcOffset($page, $containerSize);
            if ($offset >= $productsCount) {
                $page = 1;
                $offset = $this->calcOffset($page, $containerSize);
            }
            $products = Product::inRandomOrder()->limit($containerSize)->offset($offset)->get();
            $container->products()->attach($products);
            $page++;
        });
        $this->line('Seeding containers table: done');
    }

    /**
     * @param $page
     * @param $limit
     *
     * @return int
     */
    private function calcOffset($page, $limit): int
    {
        return $limit * ($page - 1);
    }
}
