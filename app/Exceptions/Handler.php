<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param Exception $exception
     *
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param Exception                $exception
     *
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function render($request, Exception $exception)
    {
        $message = $exception->getMessage();

        switch (get_class($exception)) {
            case ApiException::class:
                return parent::render($request, $exception);
            case ValidationException::class:
                $message = $exception->errors();
                $code = 422;
                break;
            case ModelNotFoundException::class:
                $code = 404;
                break;
            default:
                $code = 500;
                break;
        }

        $response = [
            'message' => $message,
        ];

        if ($request->wantsJson()) {
            if (env('APP_DEBUG')) {
                $response['trace'] = $exception->getTraceAsString();
            }
            return response()->json($response, $code);
        }
        return parent::render($request, $exception);
    }
}
