<?php

namespace App\Exceptions;

use Exception;

abstract class ApiException extends Exception
{
    protected $data;

    public function __construct($message, $code, $data = [])
    {
        $this->data = $data;
        parent::__construct($message, $code);
    }

    public function render()
    {
        return $this->jsonError();
    }

    protected function jsonError()
    {
        $response = [
            'message' => $this->getMessage(),
        ];

        if (isset($this->data)) {
            $response['data'] = $this->data;
        }

        return response()->json($response, $this->getCode());
    }
}