<?php

namespace App\Components\Products\Models;

use App\Components\Containers\Models\Container;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Product
 *
 * @property int $id
 * @property string                      $name
 * @property Carbon|null                 $created_at
 * @property Carbon|null                 $updated_at
 * @method static Builder|Product newModelQuery()
 * @method static Builder|Product newQuery()
 * @method static Builder|Product query()
 * @method static Builder|Product whereCreatedAt($value)
 * @method static Builder|Product whereId($value)
 * @method static Builder|Product whereName($value)
 * @method static Builder|Product whereUpdatedAt($value)
 * @mixin Builder
 * @property-read Collection|Container[] $containers
 */
class Product extends Model
{
    protected $fillable = [
        'name',
    ];

    /**
     * @return BelongsToMany
     */
    public function containers(): BelongsToMany
    {
        return $this->belongsToMany(
            Container::class,
            'container_product',
            'product_id',
            'container_id'
        );
    }
}
