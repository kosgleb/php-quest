<?php

namespace App\Components\Products\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductResourceCollection extends ResourceCollection
{

    public $collects = ProductResource::class;

}