<?php

namespace App\Components\Products\Http\Resources;

use App\Components\Containers\Http\Resources\ContainerResourceCollection;
use App\Components\Products\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ProductResource
 * @package App\Components\Products\Http\Resources
 * @property-read int $id
 * @property-read string $name
 */
class ProductResource extends JsonResource
{

    /** @var Product $resource */
    public $resource;

    public function toArray($request)
    {
        $resource = [
            'id' => $this->id,
            'name' => $this->name,
        ];

        if ($this->resource->relationLoaded('containers')) {
            $resource['containers'] = new ContainerResourceCollection($this->resource->containers);
        }

        return $resource;
    }

}