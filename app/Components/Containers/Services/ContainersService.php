<?php

namespace App\Components\Containers\Services;

use App\Components\Containers\Classes\Contracts\Services\ContainersService as ContainersServiceContract;
use App\Components\Containers\Models\Container;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

class ContainersService implements ContainersServiceContract
{

    /**
     * @param int $id
     *
     * @return Container|null
     *
     * @throws ModelNotFoundException
     */
    public function find(int $id): ?Container
    {
        return Container::findOrFail($id);
    }

    /**
     * @param array $filters
     *
     * @return Builder
     */
    public function filter(array $filters = []): Builder
    {
        $builder = Container::query();

        foreach ($filters as $key => $filter) {
            if (is_array($filter)) {
                $builder->whereIn($key, $filter);
            } else {
                $builder->where($key, $filter);
            }
        }

        return $builder;
    }

    /**
     * @param array $attributes
     *
     * @return Container|null
     */
    public function store(array $attributes = []): ?Container
    {
        return Container::create($attributes);
    }

    /**
     * @return Collection
     */
    public function getContainersWithDistinctProducts(): Collection
    {
        $containerIds = DB::table('products')
            ->selectRaw('any_value(container_product.container_id) container_id')
            ->distinct()
            ->join('container_product', 'products.id', '=', 'container_product.product_id')
            ->groupBy('product_id')
            ->get();
        return Container::whereIn('id', $containerIds->pluck('container_id'))->get();
    }
}