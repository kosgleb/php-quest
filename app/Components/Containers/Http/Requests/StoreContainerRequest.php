<?php

namespace App\Components\Containers\Http\Requests;

use App\Support\FormRequest;

/**
 * Class StoreContainerRequest
 * @package App\Components\Containers\Http\Requests
 *
 * @OA\RequestBody(
 *     request="StoreContainerRequest",
 *     @OA\MediaType(
 *         mediaType="multipart/form-data",
 *         @OA\Schema(
 *             required={"name"},
 *             @OA\Property(property="name", type="string", description="Наименование контейнера"),
 *             @OA\Property(
 *                 property="products",
 *                 type="array",
 *                 description="Массив id товаров",
 *                 @OA\Items(type="integer")
 *             )
 *         )
 *     )
 * ),
 */
class StoreContainerRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string',
            'products' => 'nullable|array',
            'products.*' => 'required|integer',
        ];
    }

}