<?php

namespace App\Components\Containers\Http\Requests;

use App\Support\FormRequest;

/**
 * Class IndexContainersRequest
 * @package App\Components\Containers\Http\Requests
 */
class IndexContainersRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'limit' => 'nullable|integer',
            'page' => 'nullable|integer',
        ];
    }

}