<?php

namespace App\Components\Containers\Http\Controllers\Api;

use App\Components\Containers\Classes\Contracts\Services\ContainersService;
use App\Components\Containers\Http\Requests\IndexContainersRequest;
use App\Components\Containers\Http\Requests\StoreContainerRequest;
use App\Components\Containers\Http\Resources\ContainerResource;
use App\Components\Containers\Http\Resources\ContainerResourceCollection;
use App\Http\Controllers\Controller;

class ContainersController extends Controller
{

    /** @var ContainersService $containersService */
    protected $containersService;

    public function __construct(ContainersService $containersService)
    {
        $this->containersService = $containersService;
    }

    /**
     * @param StoreContainerRequest $request
     *
     * @return ContainerResource
     *
     * @OA\Post(
     *     path="/containers",
     *     summary="Создание контейнера",
     *     tags={"Контейнеры"},
     *     @OA\RequestBody(ref="#/components/requestBodies/StoreContainerRequest"),
     *     @OA\Response(
     *         response="200",
     *         description="Контейнер создан",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Container"),
     *         )
     *     )
     * )
     */
    public function store(StoreContainerRequest $request)
    {
        $container = $this->containersService->store($request->except('products'));
        $container->products()->attach($request->get('products'));
        return new ContainerResource($container->load('products'));
    }

    /**
     * @param IndexContainersRequest $request
     *
     * @return ContainerResourceCollection
     *
     * @OA\Get(
     *     path="/containers",
     *     summary="Список контейнеров",
     *     tags={"Контейнеры"},
     *     @OA\Parameter(name="limit", schema={"type"="integer"}, in="query", example=20),
     *     @OA\Parameter(name="page", schema={"type"="integer"}, in="query", example=1),
     *     @OA\Response(
     *         response="200",
     *         description="Список контейнеров",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ContainerResourceCollection")
     *         )
     *     )
     * )
     */
    public function index(IndexContainersRequest $request)
    {
        return new ContainerResourceCollection(
            $this->containersService->filter()->paginate($request->get('limit'))
        );
    }

    /**
     * @param int $containerId
     *
     * @return ContainerResource
     *
     * @OA\Get(
     *     path="/containers/{containerId}",
     *     summary="Просмотр контейнера",
     *     tags={"Контейнеры"},
     *     @OA\Parameter(name="containerId", schema={"type"="integer"}, in="path", example=20),
     *     @OA\Response(
     *         response="200",
     *         description="Запрошенный контейнер",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Container")
     *         )
     *     )
     * )
     */
    public function show(int $containerId): ContainerResource
    {
        return new ContainerResource(
            $this->containersService->find($containerId)->load('products')
        );
    }

    /**
     * @return ContainerResourceCollection
     *
     * @OA\Get(
     *     path="/containers-with-distinct-products",
     *     summary="Список контейнеров, в которых содержатся все товары",
     *     tags={"Контейнеры"},
     *     @OA\Response(
     *         response="200",
     *         description="Список контейнеров",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(type="array", @OA\Items(ref="#/components/schemas/Container"))
     *         )
     *     )
     * )
     */
    public function getContainersWithDistinctProducts(): ContainerResourceCollection
    {
        return new ContainerResourceCollection(
            $this->containersService->getContainersWithDistinctProducts()
        );
    }

}