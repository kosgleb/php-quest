<?php

namespace App\Components\Containers\Http\Resources;

use App\Components\Containers\Models\Container;
use App\Components\Products\Http\Resources\ProductResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ContainerResource
 * @package App\Components\Containers\Http\Resources
 *
 * @property-read int $id
 * @property-read string $name
 */
class ContainerResource extends JsonResource
{

    /** @var Container $resource */
    public $resource;

    public function toArray($request)
    {
        $resource = [
            'id' => $this->id,
            'name' => $this->name,
        ];

        if ($this->resource->relationLoaded('products')) {
            $resource['products'] = new ProductResourceCollection($this->resource->products);
        }

        return $resource;
    }

}