<?php

namespace App\Components\Containers\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class ContainerResourceCollection
 * @package App\Components\Containers\Http\Resources
 *
 * @OA\Schema(
 *     schema="ContainerResourceCollection",
 *     type="object",
 *     @OA\Property(
 *         type="array",
 *         property="data",
 *         @OA\Items(
 *              ref="#/components/schemas/Container"
 *         ),
 *     ),
 *     @OA\Property(
 *         property="links",
 *         type="object",
 *         ref="#/components/schemas/PaginationLinks"
 *     ),
 *     @OA\Property(
 *         property="meta",
 *         type="object",
 *         ref="#/components/schemas/PaginationMeta"
 *     ),
 * ),
 */
class ContainerResourceCollection extends ResourceCollection
{

    public $collects = ContainerResource::class;

}