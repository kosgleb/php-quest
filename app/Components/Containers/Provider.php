<?php

namespace App\Components\Containers;

use Illuminate\Support\ServiceProvider;
use App\Components\Containers\Classes\Contracts\Services\ContainersService as ContainersServiceContract;
use App\Components\Containers\Services\ContainersService;

class Provider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind(ContainersServiceContract::class, ContainersService::class);
    }

}