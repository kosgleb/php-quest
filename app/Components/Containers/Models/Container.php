<?php

namespace App\Components\Containers\Models;

use App\Components\Products\Models\Product;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Container
 *
 * @property int $id
 * @property string                    $name
 * @property Carbon|null               $created_at
 * @property Carbon|null               $updated_at
 * @method static Builder|Container newModelQuery()
 * @method static Builder|Container newQuery()
 * @method static Builder|Container query()
 * @method static Builder|Container whereCreatedAt($value)
 * @method static Builder|Container whereId($value)
 * @method static Builder|Container whereName($value)
 * @method static Builder|Container whereUpdatedAt($value)
 * @mixin Builder
 * @property-read Collection|Product[] $products
 *
 * @OA\Schema(
 *     @OA\Property(
 *         property="id",
 *         type="integer",
 *         description="The container id"
 *     ),
 *     @OA\Property(
 *         property="name",
 *         type="string",
 *         description="The container name"
 *     )
 * )
 */
class Container extends Model
{
    protected $fillable = [
        'name',
    ];

    /**
     * @return BelongsToMany
     */
    public function products(): BelongsToMany
    {
        return $this->belongsToMany(
            Product::class,
            'container_product',
            'container_id',
            'product_id'
        );
    }
}
