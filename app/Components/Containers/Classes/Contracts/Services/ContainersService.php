<?php

namespace App\Components\Containers\Classes\Contracts\Services;

use App\Components\Containers\Models\Container;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

interface ContainersService
{

    /**
     * @param int $id
     *
     * @return Container|null
     *
     * @throws ModelNotFoundException
     */
    public function find(int $id): ?Container;

    /**
     * @param array $filters
     *
     * @return Builder
     */
    public function filter(array $filters = []): Builder;

    /**
     * @param array $attributes
     *
     * @return Container|null
     */
    public function store(array $attributes = []): ?Container;

    /**
     * @return Collection
     */
    public function getContainersWithDistinctProducts(): Collection;

}