<?php

use Laravel\Lumen\Routing\Router;

/** @var Router $router */
$router->group([
    'namespace' => 'App\Components\Containers\Http\Controllers\Api',

], function () use ($router) {
    $router->get('/containers-with-distinct-products', [
        'uses' => 'ContainersController@getContainersWithDistinctProducts',
        'as' => 'with-distinct-products.show'
    ]);
    $router->group([
        'prefix' => 'containers',
        'as' => 'containers',
    ], function () use ($router) {
        $router->get('/', [
            'uses' => 'ContainersController@index',
            'as' => 'index',
        ]);
        $router->get('/{containerId}', [
            'uses' => 'ContainersController@show',
            'as' => 'show'
        ]);
        $router->post('/', [
            'uses' => 'ContainersController@store',
            'as' => 'store',
        ]);
    });

});