<?php


namespace App\Support;

use Urameshibr\Requests\FormRequest as BaseFormRequest;

/**
 * Class FormRequest
 * @package App\Support
 *
 * @OA\Schema(
 *      schema="PaginationLinks",
 *      @OA\Property(
 *          type="string",
 *          property="first",
 *          example="http://127.0.0.1:8080/containers?page=2",
 *      ),
 *      @OA\Property(
 *          type="string",
 *          property="last",
 *          example="http://127.0.0.1:8080/containers?page=20",
 *      ),
 *      @OA\Property(
 *          type="string",
 *          property="prev",
 *          example="http://127.0.0.1:8080/containers?page=1",
 *      ),
 *      @OA\Property(
 *          type="string",
 *          property="next",
 *          example="http://127.0.0.1:8080/containers?page=2",
 *      ),
 * ),
 *
 * @OA\Schema(
 *      schema="PaginationMeta",
 *      @OA\Property(
 *          type="number",
 *          property="current_page",
 *          example=2,
 *      ),
 *      @OA\Property(
 *          type="number",
 *          property="from",
 *          example=1,
 *      ),
 *      @OA\Property(
 *          type="number",
 *          property="to",
 *          example=20,
 *      ),
 *      @OA\Property(
 *          type="number",
 *          property="last_page",
 *          example=20,
 *      ),
 *      @OA\Property(
 *          type="string",
 *          property="path",
 *          example="http://127.0.0.1:8080/containers",
 *      ),
 *      @OA\Property(
 *          type="number",
 *          property="per_page",
 *          example=20,
 *      ),
 *      @OA\Property(
 *          type="number",
 *          property="total",
 *          example=1000,
 *      ),
 * ),
 */
abstract class FormRequest extends BaseFormRequest
{
    /**
     * @return array
     */
    public function validated()
    {
        $rules = $this->container->call([$this, 'rules']);

        return $this->only(collect($rules)->keys()->map(function ($rule) {
            return explode('.', $rule)[0];
        })->unique()->toArray());
    }
}