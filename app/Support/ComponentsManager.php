<?php

namespace App\Support;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Laravel\Lumen\Application;

class ComponentsManager
{

    /**
     * @var Application
     */
    protected $app;

    /**
     * @var array
     */
    protected $components = [];

    /**
     * @var array
     */
    protected $routes = [];

    /**
     * @var array
     */
    protected $registeredComponents = [];

    /**
     * ComponentsManager constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->findComponents();
    }

    /**
     * @return array
     */
    public function getRoutes(): array
    {
        return $this->routes;
    }

    public function getComponents(): array
    {
        return $this->components;
    }

    public function getRegisteredComponents(): array
    {
        return $this->registeredComponents;
    }

    public function registerComponents(): void
    {
        foreach ($this->components as $componentName => $componentProviderClass) {
            /** @var ServiceProvider $componentProvider */
            $componentProvider = $this->app->make($componentProviderClass, [
                'app' => $this->app,
            ]);
            $this->app->register($componentProvider);
            $this->registerRoutes($componentName);
            $this->registeredComponents[] = Str::lower($componentName);
        }
    }

    protected function findComponents(): void
    {
        $components = glob(base_path(config('base.components.dir') . '*/Provider.php'));
        foreach ($components as $componentServiceProvider) {
            $componentName = last(explode('/', dirname($componentServiceProvider)));
            $this->components[$componentName] = config('base.components.namespace') . Str::ucfirst($componentName) .'\\Provider';
        }
    }

    /**
     * @param string $componentName
     */
    protected function registerRoutes(string $componentName): void
    {
        $routeFiles = glob(base_path(config('base.components.dir') . $componentName .'/Routes/*.php'));
        foreach ($routeFiles as $routeFile) {
            if (file_exists($routeFile)) {
                $this->routes[] = $routeFile;
            }
        }
    }

}