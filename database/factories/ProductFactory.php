<?php

use App\Components\Products\Models\Product;

$factory->define(Product::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
    ];
});
