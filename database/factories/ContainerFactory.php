<?php

use App\Components\Containers\Models\Container;

$factory->define(Container::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
    ];
});
